const path = require('path'),
    segmentservice = require(path.resolve('services/segment-service'))

function _getSegments(req, res) {
    let segments = segmentservice.generateSements(req.query);
    res.status(200).json(segments);
}

module.exports = {
    getSegments: _getSegments
}