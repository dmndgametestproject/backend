const path = require('path'),
    db = require(path.resolve('services/db'))

async function _getScore(req, res) {
    try {
        let _db = await db.getDB();
        let row = await _db.get();
        if (!row) {
            await _db.insert({ score: 0 })
            return res.status(200).json({score: 0});
        }
        return res.status(200).json({score: row.score});
    } catch (err) {
        return res.status(400).send();
    }
}

async function _addScore(req, res) {
    let score = req.body.score || 0;
    try {
        let _db = await db.getDB();
        let row = await _db.get();
        if (row) {
            await _db.update({ score: row.score + score })
            return res.status(200).json({score:row.score + score})
        }
        await _db.insert({ score: score })
        return res.status(200).json({score:score})
    } catch (err) {
        return res.status(400).send();
    }
}

module.exports = {
    getScore: _getScore,
    addScore: _addScore
}