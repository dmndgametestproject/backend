function _generateSements(agrs) {
    let _args = {
        min:1000,
        max:100000,
        count:16,
        val:100,
        interval:1000,
        ...agrs
    }
    let segments = [];

    do {
        let possible_value = _args.val * (_args.min/_args.val + Math.floor((_args.max-_args.min)/_args.val*Math.random()));
        if (!segments.some(s => s - _args.interval < possible_value && s + _args.interval > possible_value)) {
            segments.push(possible_value);
        }
    } while (segments.length < _args.count);

    return segments;
}

module.exports = {
    generateSements: _generateSements,
}