const sqlite3 = require('sqlite3').verbose();

var db = new sqlite3.Database(':memory:');

function _close() {
    db.close();
}

let functions = {
    get: function (cb) {
        return new Promise((resolve, reject) => {        
            let sql = `SELECT id, score
            FROM playerscore`;

            db.get(sql, [], (err, row) => {
                if (err) {
                    reject(err);
                    return console.error(err.message);
                }
                row ? console.log(row) : console.log(`No score found`);

                resolve(row);

            });
        })
    },
    insert: function (value) {
        return new Promise((resolve, reject) => {
            let sql = `INSERT INTO playerscore(id, score) VALUES(?,?)`;
            db.run(sql, [value.id ? value.id : 1, value.score], function (err) {
                if (err) {
                    reject(err);
                    return console.log(err.message);
                }
                // get the last insert id
                console.log(`A row has been inserted with rowid ${this.lastID}`);
                resolve(this.lastID);
            });
        })
    },
    update: function (value) {
        return new Promise((resolve, reject) => {
            let sql = `UPDATE playerscore
            SET score = ?
            WHERE id = ?`;

            db.run(sql, [value.score, value.id ? value.id : 1], function (err) {
                if (err) {
                    reject(err);
                    return console.error(err.message);
                }
                console.log(`Row(s) updated: ${this.changes}`);
                resolve(this.changes);
            })
        })
    }
}

function _getDB() {
    return new Promise((resolve, reject) => {
        db.run(`CREATE TABLE if not exists playerscore(id, score)`, function (err) {
            if (err) {
                reject(err);
                return console.log(err.message);
            }
            console.log(`TABLE playerscore inited`)
            resolve(functions)
        })
    })
    
}

module.exports = {
    getDB: _getDB,
    close: _close
}