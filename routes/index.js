const express = require('express'),
    path = require('path'),
    router = express.Router(),
    playerController = require(path.resolve("controllers/player-controller")),
    gameController = require(path.resolve("controllers/game-controller"));

router.get('/score', playerController.getScore);
router.post('/spin', playerController.addScore);
router.get('/segments', gameController.getSegments);

module.exports = router;