const express = require('express'),
    bodyParser = require('body-parser'),
    http = require('http'),
    cors = require('cors'),
    path = require('path'),
    db = require(path.resolve('services/db'));

const port = process.env.PORT || 3000;
const app = express();

app.use(cors());
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

app.use(require(path.resolve('routes')));

app.use((req, res, next) => {
    res.status(404).send('That route does not exist.');
});

process.on('exit', () => {
    db.close();
})

const server = http.createServer(app);

server.listen(port, () => {
    console.log(`Listening on port ${port}`);
});